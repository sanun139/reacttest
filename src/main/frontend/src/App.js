import React, {useEffect, useState} from 'react';

import axios from 'axios';

function App() {
   const [hello, setHello] = useState('')

    useEffect(() => {
        axios.get('/api/hello')
        .then(response => setHello(response.data))
        .catch(error => console.log(error))
    }, []);
    
    const[message, setMessage] = useState('')
    
    useEffect(() => {
		axios.get('/board')
		.then(response => setMessage(response.data))
		.catch(error => console.log(error))
	},[]);

    return (
        <div className='App'>
		    <header className="App-header">
		        백엔드에서 가져온 데이터입니다 : {hello}
		        board에서 가져온거 : {message}
		    </header>
        </div>
    );
}

export default App;