package com.example.demo.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Board;
import com.example.demo.service.BoardService;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class BoardController {
	
	@Autowired
	private BoardService boardService;
	
	//create board rest api
	@PostMapping("/boards")
	public Board createBoard(@RequestBody Board board) {
		return boardService.createBoard(board);
	}
	
	//list all boards
	@GetMapping("/boards")
	public java.util.List<Board> listAllBoards(){
		return boardService.listAllBoards();
	}
	
	//get board by id
	@GetMapping("/boards{id}")
	public ResponseEntity<Board> getBoardById(@PathVariable Integer id){
		return boardService.getBoradById(id);
	}
	
	//update board
	@PutMapping("/boards/{id}")
	public ResponseEntity<Board> updeteBoard(
			@PathVariable Integer id, @RequestBody Board boardDetails){
		return boardService.updateBoard(id, boardDetails);
	}
	
	//delete board
	@DeleteMapping("/boards/{id}")
	public ResponseEntity<Map<String, Boolean>> deleteBoard(@PathVariable Integer id){
		return boardService.deleteBoard(id);
	}
	
//	@GetMapping("/board")
//	public String board() {
//		System.out.println("연결 잘됨?");
//		return "연결테스트";
//	}
}
