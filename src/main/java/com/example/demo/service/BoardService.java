package com.example.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Board;
import com.example.demo.repository.BoardRepository;


@Service
public class BoardService {

	private BoardRepository boardRepository;
	
	//create board rest api
	public Board createBoard(@RequestBody Board board) {
		return boardRepository.save(board);
	}
	
	
	//list all boards
	public java.util.List<Board> listAllBoards(){
		return boardRepository.findAll();
	}
	
	//get board by id
	public ResponseEntity<Board> getBoradById(@PathVariable Integer id){
		Board board = boardRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Board not exist with id :" + id));
		int cnt = board.getViewCnt();
		board.setViewCnt(cnt + 1);
		return ResponseEntity.ok(board);
	}
	
	//update board
	public ResponseEntity<Board> updateBoard(@PathVariable Integer id, @RequestBody Board boardDeails){
		Board board = boardRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Board not exist with id :" + id));
		
		board.setTitle(boardDeails.getTitle());
		board.setContent(boardDeails.getContent());
		
		Board updeteBoard = boardRepository.save(board);
		return ResponseEntity.ok(updeteBoard);
	}
	
	//delete board
	public ResponseEntity<Map<String, Boolean>> deleteBoard(@PathVariable Integer id){
		Board board = boardRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Board not exist with id :" + id));
		
		boardRepository.delete(board);
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return ResponseEntity.ok(response);
	}
	
}
